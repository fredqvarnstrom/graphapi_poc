import adal

parameters = dict()
parameters['tenant'] = 'getcloudsafe.onmicrosoft.com'
parameters['authorityHostUrl'] = 'https://login.microsoftonline.com'
parameters['clientId'] = '0eb20a1a-10fe-405c-9203-aed64f071bd5'
parameters['clientSecret'] = 'Se2E0r7Gcbc1n//7eUc3g9nU99GSHzL8aiXp580SRcU='

authority_url = (parameters['authorityHostUrl'] + '/' + parameters['tenant'])
RESOURCE = '00000002-0000-0000-c000-000000000000'


def get_token():
    context = adal.AuthenticationContext(authority_url)

    token = context.acquire_token_with_client_credentials(
        RESOURCE,
        parameters['clientId'],
        parameters['clientSecret'])

    return token


if __name__ == '__main__':

    print get_token()['accessToken']
