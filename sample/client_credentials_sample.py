﻿import json
import adal

parameters = dict()
parameters['tenant'] = 'getcloudsafe.onmicrosoft.com'
parameters['authorityHostUrl'] = 'https://login.microsoftonline.com'
parameters['clientId'] = '0eb20a1a-10fe-405c-9203-aed64f071bd5'
parameters['clientSecret'] = 'Se2E0r7Gcbc1n//7eUc3g9nU99GSHzL8aiXp580SRcU='


authority_url = (parameters['authorityHostUrl'] + '/' + parameters['tenant'])
RESOURCE = '00000002-0000-0000-c000-000000000000'

context = adal.AuthenticationContext(authority_url)

token = context.acquire_token_with_client_credentials(
    RESOURCE,
    parameters['clientId'],
    parameters['clientSecret'])

print('Here is the token:')
print(json.dumps(token, indent=2))
