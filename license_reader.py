import urllib2
import yaml
import token_reader


token_list = token_reader.get_token()
token = token_list['accessToken']

req = urllib2.Request('https://graph.windows.net/getcloudsafe.com/subscribedSkus?api-version=1.6')
req.add_header('Authorization', token)
resp = urllib2.urlopen(req)

content = resp.read()
result = yaml.load(content)

print 'Metadata: ', result['odata.metadata']
print " --- Licenses ---"
for lic in result['value']:
    for ll in lic.items():
        print ll

    print ""


