## Install dependencies
    
1. Upgrade `pip` to the latest version. (8.1.2)

2. Install dependencies
    
        sudo pip install adal
        sudo pip install PyYAML
    